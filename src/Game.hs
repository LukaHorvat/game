{-# OPTIONS_GHC -Wno-orphans #-}
module Game where

import Interlude hiding (rotate)
import Control.Lens (Lens')
import Linear hiding (identity, rotate)
import Graphics.Gloss.Interface.Pure.Game

import Direction (Direction)
import qualified Direction as Dir

instance Semigroup Picture where
    (<>) = mappend

class Drawable a where
    draw :: a -> Picture

instance Drawable Picture where
    draw = identity

class Updateable a where
    update :: Float -> a -> a

class Eventable a where
    processEvent :: Event -> a -> a

class ToGloss a b | a -> b where
    toGloss :: a -> b

instance Real a => ToGloss (V2 a) Point where
    toGloss (V2 x y) = (realToFrac x, realToFrac y)

class HasVelocity t a | t -> a where
    velocity :: Lens' t a

class HasPosition t a | t -> a where
    position :: Lens' t a

class HasRotation t a | t -> a where
    rotation :: Lens' t a

updatePhysical :: (HasVelocity a (V2 Float), HasPosition a (V2 Float)) => Float -> a -> a
updatePhysical f a = a & position %~ (+ a^.velocity ^* f)

rotateAndPlace :: (HasRotation a (Direction Float), HasPosition a (V2 Float)) => a -> Picture -> Picture
rotateAndPlace a p = p & rotate (-a ^. rotation . Dir.angle)
                       & uncurry translate (toGloss (a ^. position))
