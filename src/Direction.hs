{-# OPTIONS_GHC -Wno-redundant-constraints #-}
module Direction where

import Interlude hiding (to)

import Data.Fixed (mod')
import Control.Lens (to, Getter)
import qualified Linear as Lin

newtype Direction a = Direction a deriving (Eq, Show, Read)

ccwFrom :: (Ord a, Num a) => Direction a -> Direction a -> Bool
(Direction x) `ccwFrom` (Direction y)
    | x > y && x - y <  180 = True
    | x < y && y - x >= 180 = True
    | otherwise             = False

ccwEqFrom :: (Ord a, Num a) => Direction a -> Direction a -> Bool
a1 `ccwEqFrom` a2 = a1 == a2 || a1 `ccwFrom` a2

cwFrom :: (Ord a, Num a) => Direction a -> Direction a -> Bool
cwFrom a b = not $ ccwEqFrom a b

cwEqFrom :: (Ord a, Num a) => Direction a -> Direction a -> Bool
cwEqFrom a b = not $ ccwFrom a b

instance (Num a, Ord a) => Ord (Direction a) where
    (<=) = ccwEqFrom

instance (Ord a, Real a) => Num (Direction a) where
    fromInteger = mkDirection . fromInteger
    (+) = error "Directions can't be added"
    (*) = error "Directions can't be multiplied"
    abs = error "Directions don't have absolute values"
    signum = error "Directions don't have signum"
    negate = error "Direction can't be negated"

instance (Ord a, Real a, Fractional a) => Fractional (Direction a) where
    fromRational = mkDirection . fromRational
    recip = error "Directions don't have a reciprocal"

mkDirection :: Real a => a -> Direction a
mkDirection ang
    | ang >= 0 && ang < 360 = Direction ang
    | otherwise             = Direction $! ang `mod'` 360

angle :: Getter (Direction a) a
angle = to (\(Direction a) -> a)

rotate :: Real a => a -> Direction a -> Direction a
rotate a (Direction b) = mkDirection (a + b)

diff :: Real a => Direction a -> Direction a -> a
diff d1 d2 | d1 <= d2  = (d1^.angle - d2^.angle) `mod'` 360
           | otherwise = (d2^.angle - d1^.angle) `mod'` 360

v2 :: Floating a => Getter (Direction a) (Lin.V2 a)
v2 = to (\d -> Lin.angle (d ^. angle / 180 * pi))
