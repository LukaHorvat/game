{-# LANGUAGE TemplateHaskell #-}
module Main where

import Interlude hiding (rotate, Down)
import Linear hiding (rotate, identity)
import Graphics.Gloss.Interface.Pure.Game
import Control.Lens
import qualified Data.Map as Map
import Data.Fixed (mod')

import Game
import Direction (Direction)
import qualified Direction as Dir

data Ship = Ship { shipPosition :: V2 Float
                 , shipRotation :: Direction Float
                 , shipVelocity :: V2 Float }

data Particle = Particle { particlePosition :: V2 Float
                         , particleRotation :: Direction Float
                         , particleVelocity :: V2 Float
                         , particleLife     :: Float }

data World = World { worldPlayer    :: Ship
                   , worldParticles :: [Particle]
                   , worldKeys      :: Map Key Bool }

makeFields ''Ship
makeFields ''World
makeFields ''Particle

instance Drawable Ship where
    draw s = line [(-10, -10), (0, 10), (10, -10), (0, 0), (-10, -10)]
        & rotate 90
        & color white
        & rotateAndPlace s

instance Updateable Ship where
    update = updatePhysical

instance Eventable Ship where
    processEvent = const identity

instance Drawable Particle where
    draw p = line [(-10, 0), (0, 0)]
        & color (withAlpha alpha (mixColors 0.3 (max 0 $ p^.life - 1.5) red yellow))
        & rotateAndPlace p
      where
         alpha = if p^.life > 1.7 then (2 - p^.life) * 3 else p^.life / 2

instance Updateable Particle where
    update f p = updatePhysical f p & life %~ subtract f

instance Drawable World where
    draw w = draw (w^.player) <> foldMap draw (w^.particles)

wantedDirection :: Map Key Bool -> Maybe (Direction Float)
wantedDirection m = case fmap fst (filter snd pairs) of
    [x] -> Just x
    xs@[_, _] | xs == [90, 180] -> Just 135
              | xs == [180, 270] -> Just 225
              | xs == [270, 0] -> Just 315
              | xs == [90, 0] -> Just 45
    _ -> Nothing
  where
    pairs = zip [90, 180, 270, 0] (m ^.. foldMap ix wasd)

instance Updateable World where
    update f w = w & player %~ update f
                   & player . velocity %~ (+ dirVec ^* thrust)
                   & player . velocity %~ (^* 0.99)
                   & player . rotation .~ rot
                   & particles . each %~ update f
                   & particles %~ filter ((> 0) . view life)
                   & particles %~ (newPart ++)
      where
        press = or (w ^.. keys . foldMap ix wasd)
        thrust = if press then 3 else 0 -- if any of wasd pressed, thrust
        p = w^.player
        rot = case wantedDirection (w^.keys) of
            Nothing -> p^.rotation
            Just d | d < p^.rotation && d `Dir.diff` (p^.rotation) > 10 ->
                       Dir.rotate 5 (p^.rotation)
                   | d > p^.rotation && d `Dir.diff` (p^.rotation) > 10 ->
                       Dir.rotate (-5) (p^.rotation)
                   | otherwise       -> d
        dirVec = p ^. rotation . Dir.v2
        partRot = Dir.rotate (rand * 10) (p^.rotation)
        partVec = partRot ^. Dir.v2
        newPart = [Particle (p^.position) partRot (p^.velocity - partVec ^* 100) 2 | press]
        rand = (((p ^. position . _x) `mod'` 1) * 100) `mod'` 1 - 0.5

instance Eventable World where
    processEvent e w = newWorld e & player %~ processEvent e
      where
        newWorld (EventKey k s _ _) = w & keys . at k .~ Just (s == Down)
        newWorld _ = w

wasd :: [Key]
wasd = fmap Char "wasd"

initialWorld :: World
initialWorld = World (Ship Linear.zero 0 (V2 10 0))
                     []
                     (Map.fromList (zip wasd (repeat False)))

main :: IO ()
main = play (InWindow "Game" (800, 500) (100, 100)) black 60 initialWorld draw processEvent update
